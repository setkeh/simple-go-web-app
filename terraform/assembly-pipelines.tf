resource "aws_iam_user" "assembly_pipelines_user" {
  name = "assembly-pipelines"
  force_destroy = true
}

resource "aws_iam_user_policy" "assembly_pipelines_user_policy" {
    name = "assembly-pipelines-user-policy"
    user = "${aws_iam_user.assembly_pipelines_user.name}"
    policy = "${file("./json/assembly-pipelines.json")}"
}
