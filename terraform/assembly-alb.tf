resource "aws_alb" "assembly_nonprod_alb" {
  name            = "assembly-nonprod"
  internal        = false
  subnets         = ["${aws_subnet.public_a_subnet.id}", "${aws_subnet.public_b_subnet.id}", "${aws_subnet.public_c_subnet.id}"]
  security_groups = [
    "${aws_security_group.egress_allow_all.id}",
    "${aws_security_group.ingress_allow_http_https.id}"
  ]
}

resource "aws_alb_listener" "http_assembly_nonprod_listener" {
  load_balancer_arn = "${aws_alb.assembly_nonprod_alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.assembly_nonprod_target_group.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "assembly_nonprod_target_group" {
  name     = "assembly-nonprod"
  port     = "8080"
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = "${aws_vpc.assembly.id}"
  deregistration_delay = 60

  health_check {
    path = "/"
    timeout = 10
    healthy_threshold = 2
    unhealthy_threshold = 3
    interval = 30
  }
}

output "target_group_arn" {
    value = "${aws_alb_target_group.assembly_nonprod_target_group.id}"
}
