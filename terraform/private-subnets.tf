resource "aws_subnet" "app_private_a_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "app_private_a_subnet")}"
  availability_zone = "ap-southeast-2a"
    tags {
        Name = "app_private_a"
    }
}

resource "aws_subnet" "app_private_b_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "app_private_b_subnet")}"
  availability_zone = "ap-southeast-2b"
    tags {
        Name = "app_private_b"
    }
}

resource "aws_subnet" "app_private_c_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "app_private_c_subnet")}"
  availability_zone = "ap-southeast-2c"
    tags {
        Name = "app_private_c"
    }
}

resource "aws_subnet" "db_private_a_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "db_private_a_subnet")}"
  availability_zone = "ap-southeast-2a"
    tags {
        Name = "db_private_a"
    }
}

resource "aws_subnet" "db_private_b_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "db_private_b_subnet")}"
  availability_zone = "ap-southeast-2b"
    tags {
        Name = "db_private_b"
    }
}

resource "aws_subnet" "db_private_c_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "db_private_c_subnet")}"
  availability_zone = "ap-southeast-2c"
    tags {
        Name = "db_private_c"
    }
}
