resource "aws_security_group" "egress_allow_all" {
    #name, name_prefix and decription deliberately not included, please use tags as identifiers
    vpc_id = "${aws_vpc.assembly.id}"

    egress = {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "egress_allow_all"
        description = "allow_outbound"
        direction = "egress"
    }
}
