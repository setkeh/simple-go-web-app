resource "aws_vpc" "assembly" {
  cidr_block = "${var.cidr_block}"
  tags { 
    Name = "assembly"
    "kubernetes.io/cluster/assembly-demo" = "shared"
  }
}
