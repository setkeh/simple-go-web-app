output "app_subnet_a" {
    value = "${aws_subnet.app_private_a_subnet.id}"
}

output "app_subnet_b" {
    value = "${aws_subnet.app_private_b_subnet.id}"
}

output "app_subnet_c" {
    value = "${aws_subnet.app_private_c_subnet.id}"
}
