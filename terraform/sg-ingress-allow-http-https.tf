resource "aws_security_group" "ingress_allow_http_https" {
    #name, name_prefix and decription deliberately not included, please use tags as identifiers
    vpc_id = "${aws_vpc.assembly.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
    tags {
        Name = "ingress_allow_http_https"
        description = "ingress_allow_http_https"
        direction = "ingress"
    }
}
