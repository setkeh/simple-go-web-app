resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.assembly.id}"
  tags { Name = "private" }
}

resource "aws_route" "private_default_route" {
    route_table_id = "${aws_route_table.private.id}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat_gw.id}"
}

resource "aws_route_table_association" "app_private_a_rta" {
    subnet_id = "${aws_subnet.app_private_a_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "app_private_b_rta" {
    subnet_id = "${aws_subnet.app_private_b_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "app_private_c_rta" {
    subnet_id = "${aws_subnet.app_private_c_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "db_private_a_rta" {
    subnet_id = "${aws_subnet.db_private_a_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "db_private_b_rta" {
    subnet_id = "${aws_subnet.db_private_b_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "db_private_c_rta" {
    subnet_id = "${aws_subnet.db_private_c_subnet.id}"
    route_table_id = "${aws_route_table.private.id}"
}
